// prettier.config.js or .prettierrc.js
// https://prettier.io/docs/en/configuration.html
module.exports = {
  trailingComma: 'es6',
  tabWidth: 4,
  semi: true,
  singleQuote: true,
};
