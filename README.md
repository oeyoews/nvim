<div align="center">
  <img src="img/vim-rainbow.png" alt="vim" align="center" width=144><hr>
  <h2>🇳  Neovim</h2>
  <p> 💡 A personal neovim configuration with UntiSnips</p>
  <hr>
</div>

<div align="center">
<img src="https://img.shields.io/badge/Neovim-0.9.0-blueviolet.svg?style=flat-square&logo=Neovim&color=90E59A&logoColor=green" alt="neovim">
<img src="https://img.shields.io/gitlab/v/tag/oeyoews/nvim?color=green&logo=FastAPI&style=flat-square" alt="tag">
<img src="https://img.shields.io/badge/Lang-lua-blueviolet.svg?style=flat-square&logo=lua&color=90E59A&logoColor=blue" alt="lang">
</div>
<hr>

## 🍾 Screenshots

| <img src="img/n1.png" align="bottom" width=256/> | <img src="img/n2.png" align="bottom" width=256/> | <img src="img/n3.png" align="bottom" width=256/> |
| :----------------------------------------------: | :----------------------------------------------: | ------------------------------------------------ |
| <img src="img/04.png" align="bottom" width=256/> | <img src="img/05.png" align="bottom" width=256/> | <img src="img/06.png" align="bottom" width=256/> |

<!-- ## 💡 What's that -->
<!---->
<!-- <a href="https://git.io/typing-svg"><img src="https://readme-typing-svg.herokuapp.com?font=FiraCode&color=63F3E1&vCenter=true&lines=A+personal+neovim+configuration+with+UltiSnips" alt="Typing SVG" /></a> -->

## 🔗 Neovim Related Links

- [convertsnippet snippet to json](https://pypi.org/project/ultisnips-vscode/)
> ultisnips2vscode

- [convert](https://github.com/VincentCordobes/convert-snippets/)
- [neovimcraft](https://neovimcraft.com/)
- [awesome](https://github.com/rockerBOO/awesome-neovim)
- [chat](https://app.element.io/#/room/#neovim:matrix.org)
